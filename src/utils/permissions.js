const { rule, shield, and, or, not } = require('graphql-shield')

const isAuthenticated = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => {
    return ctx.user !== null
  },
)

const isAdmin = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => {
    return ctx.user.role === 'admin'
  },
)

const isEditor = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => {
    return ctx.user.role === 'editor'
  },
)

// Permissions

const permissions = shield({
  Query: {
    info: not(isAuthenticated),
    feed: and(isAuthenticated, isAdmin),
  },
  Mutation: {
    post: and(isAuthenticated, isAdmin),
  },
})

module.exports = permissions;
