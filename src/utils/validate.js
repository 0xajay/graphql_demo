const jwt = require('jsonwebtoken')
const APP_SECRET = 'hsjshjsjsjhajhjdhjhdjhdjhdajkhdj'
const Users = require('../schemas/users')

const redis_client = require('./redis_client');
const { promisify } = require('util');

const client_get_async = promisify(redis_client.get).bind(redis_client);

const getUserId = async (context) => {
  const Authorization = context.request.get('Authorization')
  if (Authorization) {
    const token = Authorization.replace('Bearer ', '')
    const decoded  = jwt.verify(token, APP_SECRET)
    const redis_reply = await client_get_async(decoded.id);
    const session = JSON.parse(redis_reply);
    const user = await Users.findOne({_id:session.user_id})
    return user
  }

  throw new Error('Not authenticated')
}

module.exports = {
  APP_SECRET,
  getUserId,
}
