'use strict';

const mongoose = require('mongoose');

mongoose.Promise = require('bluebird');
const { Schema } = mongoose;

const schema = new Schema(
  {
    username: { type: String, required: true },
    password: { type: String, required: true},
    visibility: { type: Boolean, required: false, default: true }
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

module.exports = mongoose.model('users', schema);
