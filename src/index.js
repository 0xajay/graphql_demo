const { GraphQLServer } = require('graphql-yoga')

const Boom = require('@hapi/boom')
const mongoose = require('mongoose')
const Urls = require('./schemas/urls')
const Users = require('./schemas/users')
const jwt = require('jsonwebtoken');
const {APP_SECRET, getUserId } = require('./utils/validate')
const permissions = require('./utils/permissions')
const uuid = require('uuid')
const redis_client = require('./utils/redis_client');
const { promisify } = require('util');
const client_set_async = promisify(redis_client.set).bind(redis_client);
const { directiveResolvers } = require('./directive')

const resolvers = {
  Query:{
    info: () => {
      return { success: false, error: { message: 'Todo must be greater than 3 chars' } }
    },
    feed: () => {
      const urls = Urls.find({})
      return urls
    },
    link: async (parent, args) => {
      link = await Urls.findOne({_id:args.id})
      return link
    }
  },

  Mutation : {
    post: async (parent, args, context) => {
      // const user = await getUserId(context)
      const link = {
        description: args.description,
        url: args.url,
      }
      const new_link = await Urls.create(link)
      return new_link
    },
    register: async (parent, args) => {
      const payload = {
        username: args.username,
        password: args.password
      }

      const user = await Users.create(payload)
      return user
    },
    login: async (parent, args) => {
      const user = await Users.findOne({username:args.username})
      if (user){
        if (user.password == args.password){
          const session = {
            id: uuid.v4(),
            user_id: user._id,
            valid: true
          };

          // store session id into redis server
          const redis_result = await client_set_async(session.id, JSON.stringify(session));
          const token = jwt.sign(session,'hsjshjsjsjhajhjdhjhdjhdjhdajkhdj')
          return {
            token,
            username: args.username
          }
        }
        else{
          // throw new Error('Invalid Credentails')
          return Boom.badRequest('Invalid Credentails')
        }
      }else{
        throw new Error('Invalid Credentails')
      }
    }
  }
}

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  directiveResolvers,
  // middlewares: [permissions],
  context: request => {
    return {
      ...request
    }
  }
})


const init = async () => {
  await mongoose.connect(process.env.MONGODB_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
      });
  server.start(() => console.log('server is running on http;//localhost:4000'));
}

init()
