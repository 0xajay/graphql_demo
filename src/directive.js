const jwt = require('jsonwebtoken')
const APP_SECRET = 'hsjshjsjsjhajhjdhjhdjhdjhdajkhdj'
const Users = require('./schemas/users')

const redis_client = require('./utils/redis_client');
const { promisify } = require('util');

const client_get_async = promisify(redis_client.get).bind(redis_client);

const isLoggedIn = async (ctx) => {
  const Authorization = ctx.request.get('Authorization')
  if (Authorization) {
    const token = Authorization.replace('Bearer ', '')
    const decoded  = jwt.verify(token, APP_SECRET)
    const redis_reply = await client_get_async(decoded.id);
    const session = JSON.parse(redis_reply);
    const user = await Users.findOne({_id:session.user_id})
    return user.toObject()
  }

  throw new Error('Not authenticated')
}

const directiveResolvers = {
  isAuthenticated: async (next, source, args, ctx) => {
    await isLoggedIn(ctx)
    return next()
  },

  hasRole: async (next, source, { roles }, ctx) => {
    const user = await isLoggedIn(ctx)
    if (roles.includes(user.role)) {
      return next()
    }
    throw new Error(`Unauthorized, incorrect role`)
  }
}

module.exports = { directiveResolvers };
